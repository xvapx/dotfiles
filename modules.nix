{ cfg, lib, pkgs, dotfiles, secrets, hostname, ... }:

with lib;

{

  options.marti.dotfiles = {
    enable = mkEnableOption "Wether to enable Marti's dotfiles." // { default = true; };
    user = {
      name = mkOption {
        type = types.str;
        default = "marti";
      };
      full_name = mkOption {
        type = types.str;
        default = "Marti Serra";
      };
      email = mkOption {
        type = types.str;
        default = "marti.serra@protonmail.com";
      };
      home = mkOption {
        type = types.str;
        default = "/home/${cfg.user.name}";
      };
      uid = mkOption {
        type = types.nullOr types.int;
        default = null;
      };
    };
  };

  config = mkMerge [
    (mkIf (!cfg.enable) {
      marti.dotfiles.bash.enable = mkForce false;
      marti.dotfiles.gdrive.enable = mkForce false;
      marti.dotfiles.git.enable = mkForce false;
      marti.dotfiles.kde.enable = mkForce false;
      marti.dotfiles.neovim.enable = mkForce false;
      marti.dotfiles.emacs.enable = mkForce false;
      marti.dotfiles.readline.enable = mkForce false;
      marti.dotfiles.steam.enable = mkForce false;
      marti.dotfiles.tmux.enable = mkForce false;
      marti.dotfiles.xorg.enable = mkForce false;
    })
    (mkIf cfg.enable {
      assertions = [{
        assertion = (cfg.user.uid != null);
        message = "uid must be set.";
      }];
    })
  ];

  imports = [
    (import ./modules/bash.nix { inherit cfg lib pkgs dotfiles; })
    (import ./modules/emacs.nix { inherit cfg lib pkgs dotfiles; })
    (import ./modules/gdrive.nix { inherit cfg lib pkgs dotfiles secrets hostname; })
    (import ./modules/git.nix { inherit cfg lib pkgs dotfiles; })
    (import ./modules/kde.nix { inherit cfg lib pkgs dotfiles; })
    (import ./modules/neovim.nix { inherit cfg lib pkgs dotfiles; })
    (import ./modules/readline.nix { inherit cfg lib pkgs dotfiles; })
    (import ./modules/steam.nix { inherit cfg lib pkgs dotfiles; })
    (import ./modules/tmux.nix { inherit cfg lib pkgs dotfiles; })
    (import ./modules/xorg.nix { inherit cfg lib pkgs dotfiles; })
  ];

}
