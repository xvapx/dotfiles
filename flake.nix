{
  description = "Marti's dotfiles";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }: {
    # Export files directly as nix function to bypass module
    dotfiles = { pkgs }: import ./dotfiles.nix {
      inherit pkgs;
    };
    # Export as a module
    nixosModules = {
      module = {
        imports = [
          ./module.nix
        ];
      };
    };
    nixosModule = self.nixosModules.module;
  };
}
