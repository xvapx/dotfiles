{ lib }:

{
# This function allows users to create files outside of the nix store on system activation (rebuild and boot)
writeFile = {
  source ? null,      # copy this file or directory, incompatible with text
  target,             # where to write the file or directory
  text ? null,        # content of the file, incompatible with source
  mode ? "0711",      # target permissions, null to not modify them
  user,               # target owner name
  group ? "users",    # target group name
  overwrite ? false,  # overwrite target if it already exists?
  backup ? true       # backup existing target before overwriting?
}:
# check that only one of source or text are supplied
assert lib.assertMsg (source != null || text != null) "Only one of source or text must be supplied.";
{
  text = ''
    # create the parent directory if needed
    mkdir -p "$(dirname "${target}")"
    # check if the target already exists
    if [ -e "${target}" ]; then
      ${if !overwrite then ''
        # exit without overwriting
        exit 0
      '' else if backup then ''
        # backup
        mv "${target}" "${target}.bak"
      '' else ''
        # remove target
        rm -rf "${target}"
      '' }
      fi
    # create the target
    ${if source != null then ''
      cp -r "${source}" "${target}"''
    else ''
      echo -n '${lib.escapeShellArg text}' > "${target}"''
    }
    # set owner
    chown -R ${user}:${group} "${target}"
    # change permissions if needed
    ${if mode != null then ''
      chmod -R ${mode} "${target}"
    '' else ""}
  '';
  };
}

