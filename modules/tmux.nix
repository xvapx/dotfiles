{ cfg, lib, pkgs, dotfiles, ... }:

with lib;

rec {

  options.marti.dotfiles.tmux = {
    enable = mkEnableOption "Enable tmux?" // { default = true; };
    directories.tmp = mkOption {
      type = types.path;
      default = "/unr/user/${toString cfg.user.uid}";
      description = "TMUX_TMPDIR variable content.";
    };
  };

  config = mkIf cfg.tmux.enable {
    users.users.${cfg.user.name}.packages = with pkgs; [
      tmux
    ];
    home-manager.users.${cfg.user.name}.home = {
      file.".tmux.conf".text = dotfiles.tmux.conf;
      sessionVariables.TMUX_TMPDIR = cfg.tmux.directories.tmp;
    };
  };

}
