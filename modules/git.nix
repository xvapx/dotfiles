{ cfg, lib, pkgs, dotfiles, ... }:

with lib;

rec {

  options.marti.dotfiles.git = {
    enable = mkEnableOption "Enable git?" // { default = true; };
    signing = {
      enable = mkEnableOption "sign commits with gpg?";
      key = mkOption {
        type = types.str;
        default = "";
        description = "gpg signing key";
      };
    };
    editor = mkOption {
      type = types.str;
      default = "vim";
      description = "default editor";
    };
  };

  config = mkIf cfg.git.enable {
    users.users.${cfg.user.name}.packages = with pkgs; [
      git
    ];
    home-manager.users.${cfg.user.name}.home.file.".config/git/config".text = dotfiles.git.config {
      email = cfg.user.email;
      name = cfg.user.full_name;
      sign = cfg.git.signing.enable;
      sign-key = cfg.git.signing.key;
      editor = cfg.git.editor;
    };
  };

}
