{ cfg, lib, pkgs, dotfiles, ... }:

with lib;

rec {

  options.marti.dotfiles.readline = {
    enable = mkEnableOption "Enable readline customisation?" // { default = true; };
  };

  config = mkIf cfg.readline.enable {
    home-manager.users.${cfg.user.name}.home.file.".inputrc".text = dotfiles.readline.inputrc;
  };

}
