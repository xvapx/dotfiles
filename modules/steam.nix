{ cfg, lib, pkgs, dotfiles, ... }:

with lib;

rec {

  options.marti.dotfiles.steam = {
    enable = mkEnableOption "Enable steam?";
    autostart = mkEnableOption "Enable steam autostart?" // { default = true; };
  };

  config = mkMerge [
    (mkIf cfg.steam.enable {
      programs.steam.enable = true;
    })
    (mkIf (cfg.steam.enable && cfg.steam.autostart) {
      home-manager.users.${cfg.user.name}.home.file.".config/autostart/steam.desktop".text = dotfiles.steam.desktop;
    })
  ];

}
