{ cfg, lib, pkgs, dotfiles, ... }:

with lib;

rec {

  options.marti.dotfiles.kde = {
    enable = mkEnableOption "Configure xorg applications?" // { default = true; };
    baloo = {
      enable = mkEnableOption "Enable baloo file indexing?" // { default = true; };
      hidden = mkEnableOption "Index hidden folders?";
    };
  };

  config = mkIf cfg.kde.enable {
    home-manager.users.${cfg.user.name}.home = {
      file.".config/baloofilerc".text = with cfg.kde; dotfiles.kde.baloofilerc {
        indexing = baloo.enable;
        hidden = baloo.hidden;
        home = cfg.user.home;
      };
    };
  };

}
