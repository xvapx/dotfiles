{ cfg, lib, pkgs, dotfiles, ... }:

with lib;

rec {

  options.marti.dotfiles.emacs = {
    enable = mkEnableOption "Enable emacs?" // { default = true; };
    default_editor = mkEnableOption "make emacs the default editor?";
    spacemacs = mkEnableOption "Auto-install spacemacs?" // { default = true; };
  };

  config = mkIf cfg.emacs.enable {
    users.users.${cfg.user.name}.packages = with pkgs; [
      emacs
    ] ++ (if cfg.emacs.spacemacs then [
      # spacemacs dependencies
      git
      gnutar
      # package dependencies
      libgcc
      libtool
      libvterm
      python3
    ] else []);
    fonts.packages = with pkgs; [] ++ (if cfg.emacs.spacemacs then [
      # spacemacs dependencies
    ] else [
      source-code-pro
      nanum-gothic-coding
      emacs-all-the-icons-fonts
    ]);

    system.userActivationScripts = {
      "install-spacemacs_d-${cfg.user.name}" = let
        spacemacs = builtins.fetchGit {
          url = "https://github.com/syl20bnr/spacemacs";
          rev = "2dadee57110fc9db43756fcad08af9ac3ae7c9fa";
        };
      in dotfiles.lib.writeFile {
        target = "${cfg.user.home}/.emacs.d";
        source = spacemacs;
        user = "${cfg.user.name}";
        mode = "u+w";
      };
      "install-spacemacs-${cfg.user.name}" = dotfiles.lib.writeFile {
        target = "${cfg.user.home}/.spacemacs";
        source = ../dotfiles/spacemacs/spacemacs.el;
        user = "${cfg.user.name}";
        mode = "0711";
      };
    };

    home-manager.users.${cfg.user.name}.home = {
      sessionVariables.EDITOR = if cfg.emacs.default_editor then "emacs" else mkDefault "";
    };
  };

}
