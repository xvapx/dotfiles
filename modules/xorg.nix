{ cfg, lib, pkgs, dotfiles, ... }:

with lib;

rec {

  options.marti.dotfiles.xorg = {
    enable = mkEnableOption "Configure xorg applications?" // { default = true; };
    urxvt = mkEnableOption "Configure urxvt?" // { default = true; };
  };

  config = mkIf cfg.xorg.enable {
    home-manager.users.${cfg.user.name}.home = {
      file.".Xdefaults".text = dotfiles.xorg.xdefaults {
        urxvt = cfg.xorg.urxvt;
      };
      file.".Xresources".text = dotfiles.xorg.xresources {
        urxvt = cfg.xorg.urxvt;
      };
    };
  };

}
