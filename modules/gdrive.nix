{ cfg, lib, pkgs, dotfiles, secrets, hostname, ... }:

with lib;

rec {

  options.marti.dotfiles.gdrive = {
    enable = mkEnableOption "Enable gdrive manual sync?";
    login = mkEnableOption "Enable gdrive sync on graphical login?" // { default = true; };
    hosts = mkOption {
      type = types.listOf types.str;
      default = [];
      description = "List of hosts in which to enable this module, all hosts if the list is empty.";
    };
    dir = mkOption {
      type = types.path;
      default = "";
      description = "Directory to sync.";
    };
    api = {
      id = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "API id to use.";
      };
      key = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "API secret to use.";
      };
    };
  };

  config = mkMerge [
    (mkIf (cfg.gdrive.hosts != [] && !(builtins.elem hostname cfg.gdrive.hosts)) {
      marti.dotfiles.gdrive.enable = mkForce false;
    })
    (mkIf cfg.gdrive.enable {
      assertions = [
        {
          assertion = (cfg.gdrive.api.id != null || (secrets ? marti_gdrive_id));
          message = "gdrive.api.id or age.secrets.marti_gdrive_id must be set.";
        }
        {
          assertion = (cfg.gdrive.api.key != null || (secrets ? marti_gdrive_key));
          message = "gdrive.api.key or age.secrets.marti_gdrive_key must be set.";
        }
      ];
      marti.dotfiles.gdrive.dir = mkDefault "${cfg.user.home}/gdrive";
      home-manager.users.${cfg.user.name}.home.file = {
        "/bin/gdrive" = {
          text = dotfiles.bin.gdrive {
            api = {
              id =
                if secrets ? marti_gdrive_id then
                  ''''$(cat "${secrets.marti_gdrive_id.path}")'' else cfg.gdrive.api.id;
              secret =
                if secrets ? marti_gdrive_key then
                  ''''$(cat "${secrets.marti_gdrive_key.path}")'' else cfg.gdrive.api.key;
            };
            dir = cfg.gdrive.dir;
          };
          executable = true;
        };
        "${cfg.gdrive.dir}/.keep".text = "Automatically generated to make sure this directory exists.";
      };
    })
    (mkIf (cfg.gdrive.enable && cfg.gdrive.login) {
      home-manager.users.${cfg.user.name}.home.file.".config/autostart/gdrive.desktop".text = ''
        [Desktop Entry]
        Exec=${cfg.user.home}/bin/gdrive
        Icon=system-run
        Terminal=true
        Type=Application
        # Hide this entry in all menus
        NoDisplay=true
      '';
    })
  ];

}
