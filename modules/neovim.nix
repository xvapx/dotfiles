{ cfg, lib, pkgs, dotfiles, ... }:

with lib;

rec {

  options.marti.dotfiles.neovim = {
    enable = mkEnableOption "Enable neovim?" // { default = true; };
    default_editor = mkEnableOption "make neovim the default editor?";
  };

  config = mkIf cfg.neovim.enable {
    users.users.${cfg.user.name}.packages = with pkgs; [
      neovim
    ];
    home-manager.users.${cfg.user.name}.home = {
      file.".config/nvim/init.vim".text = dotfiles.neovim.init {
        home = cfg.user.home;
      };
      sessionVariables.EDITOR = if cfg.neovim.default_editor then "nvim" else mkDefault "";
    };
  };

}
