{ cfg, lib, pkgs, dotfiles, ... }:

with lib;

rec {

  options.marti.dotfiles.bash = {
    enable = mkEnableOption "Enable bash customisation?" // { default = true; };
    extra = {
      rc.pre = mkOption {
        type = types.lines;
        default = "";
        description = "Configuration to prepend to bashrc.";
      };
      rc.post = mkOption {
        type = types.lines;
        default = "";
        description = "Configuration to append to bashrc.";
      };
      path.pre = mkOption {
        type = types.str;
        default = "";
        description = "Path to prepend.";
      };
      path.post = mkOption {
        type = types.str;
        default = "";
        description = "Path to append.";
      };
      aliases.pre = mkOption {
        type = types.lines;
        default = "";
        description = "Configuration to prepend to bash_aliases.";
      };
      aliases.post = mkOption {
        type = types.lines;
        default = "";
        description = "Configuration to append to bash_aliases.";
      };
    };
    direnv.enable = mkEnableOption "Enable direnv?" // { default = true; };
    fasd.enable = mkEnableOption "Enable fasd?" // { default = true; };
    git.enable = mkEnableOption "Enable git inegration?" // { default = true; };
    ssh-agent.start = mkEnableOption "Auto-start ssh-agent?";
    tmux.start = mkEnableOption "Auto-start tmux?";
    virtualization.aliases = mkEnableOption "Add virtualization-related aliases?";
  };

  config = mkIf cfg.bash.enable (mkMerge [ {
    home-manager.users.${cfg.user.name}.home.file = {
      ".bash_profile".text = dotfiles.bash.profile;
      ".bashrc".text = with cfg.bash; dotfiles.bash.rc {
        pre = extra.rc.pre;
        post = extra.rc.post;
        tmux = tmux.start;
        path-pre = extra.path.pre;
        path-post = extra.path.post;
        direnv = direnv.enable;
        fasd = fasd.enable;
        git = git.enable;
        ssh-agent = ssh-agent.start;
      };
      ".bash_aliases".text = with cfg.bash; dotfiles.bash.aliases {
        pre = extra.aliases.pre;
        post = extra.aliases.post;
        git = git.enable;
        fasd = fasd.enable;
        virtualization = virtualization.aliases;
        home = cfg.user.home;
      };
      ".bash_locale".text = dotfiles.bash.locale;
    };
  }
    ( mkIf cfg.bash.direnv.enable {
      home-manager.users.${cfg.user.name}.home.file = {
        ".config/direnv/direnvrc".text = ''
          source /run/current-system/sw/share/nix-direnv/direnvrc
        '';
      };
    })
  ]);

}
