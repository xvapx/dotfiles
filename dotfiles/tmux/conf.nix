{ pkgs }:

''
  # $TERM contents
  set -g default-terminal "tmux-256color"

  # rebind main key: C-Space
  unbind C-b
  set -g prefix C-Space
  bind C-Space send-prefix

  # vi-style keybindings
  set -g status-keys vi
  set -g mode-keys vi

  # Base-index
  set -g base-index 1
  setw -g pane-base-index 1

  # Expand window to maximum size allowed for clients displaying it
  setw -g aggressive-resize on

  # 24H clock
  setw -g clock-mode-style  24

  # ESC key works instantaniously
  set -s escape-time 100

  # big history
  set -g history-limit 10000

  # Automatically set window and terminal title
  setw -g automatic-rename on
  set-option -g set-titles on

  # Automatically renumber windows
  set -g renumber-windows on

  # Monitor window activity
  setw -g monitor-activity on

  # Longer indication times
  set -g display-panes-time 800
  set -g display-time 1000

  # Split panes using ¡ and -
  bind ¡ split-window -h
  bind - split-window -v

  # Switch panes using Alt-arrow without prefix
  bind -n M-Left select-pane -L
  bind -n M-Right select-pane -R
  bind -n M-Up select-pane -U
  bind -n M-Down select-pane -D

  # Shift arrow to switch windows without prefix
  bind -n S-Left  previous-window
  bind -n S-Right next-window

  # Move windows
  bind -r < swap-window -t -
  bind -r > swap-window -t +

  # Enable session & client locking
  set -g lock-command vlock
  set -g lock-after-time 0 # never
  bind l lock-client
  bind L lock-session

  # clear both screen and history
  bind -n C-l send-keys C-l \; run 'sleep 0.1' \; clear-history

  # synchronize panes
  bind = setw synchronize-panes

  ## Status bars

  # Monokai is best theme.
  setw -g clock-mode-colour colour187 # monokai yellow
  setw -g window-status-style bg=colour237,fg=colour187,dim
  setw -g window-status-current-style bg='#000000',fg=colour161,dim
  set -g status-style bg=colour237,fg=colour187
  set -g pane-active-border-style bg='#101010',fg='#87d700'
  set -g pane-border-style bg='#101010',fg='#505050'
  set -g mode-style bg='#000000',fg=colour187
  set -g message-style bg='#000000',fg=colour187

  # set update frequency (default 15 seconds)
  set -g status-interval 1

  ## Customize status bar

  # Window list
  set -g status-justify centre
  setw -g window-status-format '[ #I: #W ]'
  setw -g window-status-current-format '[ #I: #W ]'

  # Left side
  set -g status-left-length 70
  #S - Session name
  #P - Pane index
  #D - Pane ID
  #T - Pane title
  set -g status-left '#[fg=colour187]#{prefix_highlight} #S | #P-#D - #T'

  # Right side
  #H - Local hostname
  set -g status-right '#(who | cut -d " " -f1)@#H | %H:%M | %Y-%m-%d'

  ## Plugins

  # Restore sessions with tmux-resurrect
  run-shell ${pkgs.tmuxPlugins.resurrect}/share/tmux-plugins/resurrect/resurrect.tmux
  set -g @resurrect-dir '~/.tmux/resurrect'
  set -g @resurrect-capture-pane-contents 'on'
  set -g @resurrect-strategy-nvim 'session'

  # Copy to the system clipboard in tmux
  run-shell ${pkgs.tmuxPlugins.yank}/share/tmux-plugins/yank/yank.tmux

  # Better tmux search
  run-shell ${pkgs.tmuxPlugins.copycat}/share/tmux-plugins/copycat/copycat.tmux
  set -g @copycat_prev 'p'

  # Tmux prefix and copy mode highlight
  run-shell ${pkgs.tmuxPlugins.prefix-highlight}/share/tmux-plugins/prefix-highlight/prefix_highlight.tmux
  set -g @prefix_highlight_show_copy_mode 'on'
  set -g @prefix_highlight_copy_mode_attr 'fg=yellow,bg=default,bold'

  # Lightweight tmux utilities for manipulating tmux sessions
  run-shell ${pkgs.tmuxPlugins.sessionist}/share/tmux-plugins/sessionist/sessionist.tmux

  # Tmux Sidebar
  run-shell ${pkgs.tmuxPlugins.sidebar}/share/tmux-plugins/sidebar/sidebar.tmux
  set -g @sidebar-tree-command 'tree -C'
''
