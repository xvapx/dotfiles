''
  export LANG=en_GB.UTF-8
  export LANGUAGE=en
  export LC_ADDRESS=ca_ES.UTF-8
  export LC_COLLATE=ca_ES.UTF-8
  export LC_MEASUREMENT=ca_ES.UTF-8
  export LC_MONETARY=ca_ES.UTF-8
  export LC_NUMERIC=ca_ES.UTF-8
  export LC_PAPER=ca_ES.UTF-8
  export LC_TIME=ca_ES.UTF-8
''
