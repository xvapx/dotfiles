{ pkgs
, pre ? ""
, post ? ""
, tmux ? false
, path-pre ? ""
, path-post ? ""
, direnv ? false
, fasd ? false
, git ? false
, ssh-agent ? false }:

pre + ''
  ### INTERACTIVE ##############################################################
  #
  # If not running interactively, don't do anything!
  if [[ $- != *i* ]]; then
    return
'' + (if !tmux then "" else ''
  else
    # If we are not inside screen or tmux and $NOTMUX is not set,
    if command -v ${pkgs.tmux}/bin/tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ] && [ -z "$NOTMUX" ]; then
      # auto-attach to an existing tmux session or auto-create one.
      exec ${pkgs.tmux}/bin/tmux new-session -A -s default
    fi
'') + ''
  fi
  #
  ### /INTERACTIVE #############################################################

  ### XDG ######################################################################
  #
  # Automatically export all variables defined from here on.
  set -a
  # Export XDG directories, respecting the value if it's already set.
  : "''${XDG_CACHE_HOME:=$HOME/.cache}"
  : "''${XDG_CONFIG_HOME:=$HOME/.config}"
  : "''${XDG_DATA_HOME:=$HOME/.local/share}"
  : "''${XDG_STATE_HOME:=$HOME/.local/state}"
  : "''${XDG_RUNTIME_DIR:=/var/run/$UID}"
  # Stop automatically exporting all variables defined from here on.
  set +a
  #
  ### /XDG #####################################################################

  ### HISTORY ##################################################################
  #
  # Make sure the directory exists
  HISTDIR=$XDG_DATA_HOME/bash
  mkdir -p "$HISTDIR"
  # History File
  export HISTFILE=$HISTDIR/history
  # Ignore commands starting with a space and duplicates
  HISTCONTROL=ignorespace:ignoredups:erasedups
  # Set a big history with timestamps
  export HISTFILESIZE=10000
  export HISTSIZE=10000
  export HISTTIMEFORMAT="[%F %T] "
  # Write history after every command
  PROMPT_COMMAND="history -a; $PROMPT_COMMAND"
  # Append to the history file instead of overwriting it
  shopt -s histappend
  #
  ### /HISTORY #################################################################

  ### LOCALE ###################################################################
  #
  # import the locale file if it exists
  if [ -f ~/.bash_locale ]; then
    . ~/.bash_locale
  fi
  #
  ### /LOCALE ##################################################################

  ### ALIASES ##################################################################
  #
  # Import aliases
  if [ -f ~/.bash_aliases ]; then
  . ~/.bash_aliases
  fi
  #
  ### /ALIASES #################################################################

  ### CD #######################################################################
  #
  # If the command is the name of a directory, cd to it
  shopt -s autocd
  #
  # Autocorrect minor spelling errors in cd
  shopt -s cdspell
  #
  ### /CD ######################################################################

  ### PATH #####################################################################
  #
  # add cfg.bash.extra.path.pre, ~/bin and cfg.bash.extra.path.post to path.
  export PATH=${path-pre}~/bin:$PATH${path-post}
  #
  ### /PATH ####################################################################

  ### STYLES ###################################################################
  #
  # foreground (text) colors
  BLACK="\[\e[30m\]"
  RED="\[\e[31m\]"
  GREEN="\[\e[32m\]"
  YELLOW="\[\e[33m\]"
  BLUE="\[\e[34m\]"
  MAGENTA="\[\e[35m\]"
  CYAN="\[\e[36m\]"
  LIGHT_GRAY="\[\e[37m\]"
  DARK_GRAY="\[\e[90m\]"
  LIGHT_RED="\[\e[91m\]"
  LIGHT_GREEN="\[\e[92m\]"
  LIGHT_YELLOW="\[\e[93m\]"
  LIGHT_BLUE="\[\e[94m\]"
  LIGHT_MAGENTA="\[\e[95m\]"
  LIGHT_CYAN="\[\e[96m\]"
  WHITE="\[\e[97m\]"
  #
  # background colors
  BG_BLACK="\[\e[40m\]"
  BG_RED="\[\e[41m\]"
  BG_GREEN="\[\e[42m\]"
  BG_YELLOW="\[\e[43m\]"
  BG_BLUE="\[\e[44m\]"
  BG_MAGENTA="\[\e[45m\]"
  BG_CYAN="\[\e[46m\]"
  BG_LIGHT_GRAY="\[\e[47m\]"
  BG_DARK_GRAY="\[\e[100m\]"
  BG_LIGHT_RED="\[\e[101m\]"
  BG_LIGHT_GREEN="\[\e[102m\]"
  BG_LIGHT_YELLOW="\[\e[103m\]"
  BG_LIGHT_BLUE="\[\e[104m\]"
  BG_LIGHT_MAGENTA="\[\e[105m\]"
  BG_LIGHT_CYAN="\[\e[106m\]"
  BG_WHITE="\[\e[107m\]"
  #
  # text formatting
  BOLD="\e[1m\]"
  DIM="\e[2m\]"
  UNDERLINED="\e[4m\]"
  BLINK="\e[5m\]"
  INVERTED="\e[7m\]"
  HIDDEN="\e[8m\]"
  # reset both formatting and color
  RESET="\e[0m\]" 
  #
  # Add color to less
  export LESS_TERMCAP_mb=$'\e[1;32m'
  export LESS_TERMCAP_md=$'\e[1;32m'
  export LESS_TERMCAP_me=$'\e[0m'
  export LESS_TERMCAP_se=$'\e[0m'
  export LESS_TERMCAP_so=$'\e[01;33m'
  export LESS_TERMCAP_ue=$'\e[0m'
  export LESS_TERMCAP_us=$'\e[1;4;31m'
  #
  ### /STYLES ##################################################################

'' + (if !direnv then "" else ''
  ### DIRENV ###################################################################
  #
  # hook direnv
  eval "$(${pkgs.direnv}/bin/direnv hook bash)"
  #
  ### /DIRENV ##################################################################

'') + (if !fasd then "" else ''
  ### FASD #####################################################################
  #
  # init fasd
  eval "$(${pkgs.fasd}/bin/fasd --init auto)"
  #
  ### /FASD ####################################################################

'') + (if !git then "" else ''
  ### GIT ######################################################################
  #
  # get current status of git repo
  parse_git_status() {
    ${pkgs.git}/bin/git status 2>/dev/null | (
      unset dirty deleted untracked newfile ahead renamed
      while read -r line; do
        case "$line" in
          *modified:*) dirty='!'; ;;
          *deleted:*) deleted='x'; ;;
          *'Untracked files:') untracked='?'; ;;
          *'new file:') newfile='+'; ;;
          *'Your branch is ahead of '*) ahead='*'; ;;
          *renamed:*) renamed='>'; ;;
        esac
      done
      bits="$dirty$deleted$untracked$newfile$ahead$renamed"
      [ -n "$bits" ] && echo " $bits"
    )
  }
  #
  # get current branch in git repo
  parse_git_branch() {
    BRANCH=$(${pkgs.git}/bin/git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
    if [ ! "''${BRANCH}" == "" ]
    then
      STAT=$(parse_git_status)
      echo "[ ''${BRANCH}''${STAT} ]"
    else
      echo ""
    fi
  }
  #
  ### /GIT #####################################################################

'') + (if !ssh-agent then "" else ''
  ### SSH-AGENT ################################################################
  #
  # Start ssh-agent or load previously started one.
  if ! pgrep -u "$USER" ssh-agent > /dev/null; then
     ssh-agent > "$XDG_RUNTIME_DIR/ssh-agent.env"
  fi
  if [[ ! "$SSH_AUTH_SOCK" ]]; then
      eval "$(<"$XDG_RUNTIME_DIR/ssh-agent.env")"
  fi
  #
  ### /SSH-AGENT ###############################################################

'') + ''
  ### PROMPT ###################################################################
  #
  # Highlight the user name when logged in as root.
  if [[ "''${USER}" == "root" ]]; then
    PROMPT_USERNAME="''${RED}\u''${RESET}";
  else
    PROMPT_USERNAME="''${GREEN}\u''${RESET}";
  fi;
  #
  # Show the hostname when connected via SSH.
  if [[ "''${SSH_TTY}" ]]; then
    PROMPT_HOSTNAME="@''${RED}\h''${RESET}";
  else
    PROMPT_HOSTNAME="";
  fi;
  #
  # Show the full path
  PROMPT_PATH="\w"
  #
  '' + (if !git then "" else ''
  # Show the git branch and status
  PROMPT_GIT=\`parse_git_branch\`
  #
  '') + ''
  # Prompt timestamp
  PROMPT_TIMESTAMP="''${GRAY}\D{%Y-%m-%d %H:%M:%S}''${RESET}";
  #
  # define the prompt
  PS1="
  ''${PROMPT_USERNAME}''${PROMPT_HOSTNAME} ''${PROMPT_TIMESTAMP} ''${PROMPT_PATH} '' +
  (if !git then "" else '' ''${PROMPT_GIT}'') + ''

  \\$ "
  #
  ### /PROMPT ##################################################################
'' + post
