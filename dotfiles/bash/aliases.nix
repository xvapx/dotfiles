{ pkgs, pre ? "", post ? "", git ? false, fasd ? false, virtualization ? false, home ? "$HOME" }:

pre + ''
  # editors
  alias nano='nano -l -w'
  alias subl='sublime'
  alias v='nvim'
  alias h='hx'
  alias e='emacs -nw'

  # verbose defaults
  alias cp='cp -v'
  alias cv='rsync -ah --info=progress2'
  alias mv='mv -v'
  alias rm='rm -v'
  alias rr='shred -uzv'
  alias mkdir='mkdir -pv'
  alias rmdir='rmdir -v'

  # ls aliases
  alias l='ls -Fp'
  alias la='ls -FAp'
  alias ll='ls -FlAhp'

  # less settings
  export LESS='--ignore-case --status-column --LONG-PROMPT --RAW-CONTROL-CHARS --HILITE-UNREAD --tabs=2 --window=-4'

  # cd
  alias ..='cd ..'
  alias .2='cd ../..'
  alias .3='cd ../../..'
  alias .4='cd ../../../..'
  alias .5='cd ../../../../..'
  alias ~='cd ~'
  cl() { cd "$1" && ls -Fp; }
  mkcd() { mkdir "$1" && cd "$1" || exit; }

  # history
  alias gh='history | ${pkgs.ripgrep}/bin/rg'

  # disk space
  alias df='df -h'
  alias diskspace='du -Sh | sort -h -r | less'

  # rotate tty (0=0,1=90,2=180,3=270)
  rotate() { echo "$1" | sudo tee /sys/class/graphics/fbcon/rotate_all; }

  # vimwiki
  alias vw='nvim -c VimwikiIndex'
  gw() { ${pkgs.ripgrep}/bin/rg "$1" "${home}/gdrive/IT/vimwiki"; }

  # count files recursively
  count() { find "$1" -type f | wc -l; }

  # network
  alias ports='netstat -tulanp'

  # sudo su
  alias susu='sudo su'

  # Launch process in background with nohup & redirecting output to /dev/null.
  # Passes all the arguments.
  noh() {
    nohup "''${@}" &>/dev/null &
  }
  # autocomplete noh with commands
  complete -c noh

  # Pause a process by name
  pause() {
    kill -SIGSTOP "$(pidof "$1")"
  }
  # Continue a process by name
  unpause() {
    kill -SIGCONT "$(pidof "$1")"
  }
  # TODO: add a suspend() using criu

  # calculator, declared as function for shellcheck's benefit
  # https://github.com/koalaman/shellcheck/issues/2347
  function =() {
    local IFS=' '
    local calc="$*"
    bc -l <<<"scale=10;$calc"
  }

  # swap two files
  swap() {
    local TMPFILE=tmp.''$''$
    mv "$1" $TMPFILE && mv "$2" "$1" && mv $TMPFILE "$2"
  }

  # units
  alias u='units'

  # spellcheck
  sp() { echo "$2" | aspell -d "$1" -a; }
  spf() { aspell -c "$2" -d "$1"; }

  # text conversions
  2doc() { soffice --headless doc "$1"; }
  2odt() { soffice --headless odt "$1"; }
  2pdf() { soffice --headless pdf "$1"; }
  # image conversions
  2jpg() { fname="''${1%.*}"; convert "$1" "$fname.jpg"; }
  2png() { fname="''${1%.*}"; convert "$1" "$fname.png"; }

  # youtube-dl
  ytdl() { ${pkgs.yt-dlp}/bin/yt-dlp -i -f best --all-subs --embed-subs --embed-thumbnail --add-metadata --yes-playlist "$@"; }

'' + (if !git then "" else ''
  # git
  alias gf='git fetch'
  alias gfp='git fetch --prune'
  alias gfs='git fetch && git status'
  alias ga='git add'
  alias gaa='git add --all'
  alias gc='git commit'
  alias gcm='git commit -m'
  alias gac='git add --all && git commit --message'
  alias gps='git push'
  alias gpl='git pull'
  alias gplr='git pull --recurse-submodules'
  alias gch='git checkout'
  alias gchr='git checkout --recurse-submodules'
  alias gri='git rebase -i'
  grih() { git rebase -i HEAD~"$1"; }
  alias gb='git branch'
  alias gm='git merge --no-ff'
  gd() { git diff ''${1:-}; } # Do not doble quote or it will no longer work without arguments
  gdh() { git diff "HEAD~''${1:-}"; }
  alias gst='git status'
  alias gl='git log'
  gr() { git log --graph --abbrev-commit --decorate --format=format:'%C(auto)%h - %C(green)(%ar)%C(auto) %s - %C(dim white)%an%C(auto)%d' --all; }

'') + (if !fasd then "" else ''
  # fasd aliases
  alias fd='fasd -d'
  alias ff='fasd -f'
  alias j='fasd_cd -d'
  jl() { fasd_cd -d "$1" && ls -Fp; }
  jg() { fasd_cd -d "$1" && git fetch && git status; }
  jgl() { jg "$1" && ls -Fp; }
  alias fn='fasd -f -e nano'
  alias fs='fasd -f -e subl'
  alias fv='fasd -f -e vim'
  alias ds='fasd -d -e subl'
  alias dv='fasd -d -e vim'
  alias dt='fasd -d -e thunar'

'') + (if !virtualization then "" else ''
  # virtualization
  # launch virt-manager and connect to local qemu
  alias vml='virt-manager --connect=qemu:///system'
  # launch virt-manager and connect to local qemu with debug enabled
  alias vmld='virt-manager --debug --connect=qemu:///system'
  # launch virsh and connect to local qemu
  alias vmlc='virsh -c qemu:///system'

'') + post
