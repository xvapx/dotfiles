{ urxvt ? false }:

''
!!!
!!! General Appearance
!!!

!! Fonts
*.font: xft:Hack:style=Regular:size=10
*.boldFont: xft:Hack:style=Bold:size=10
*.italicFont: xft:Hack:style=Italic:size=10
*.boldItalicFont: xft:Hack:style=Bold Italic:size=10

!! Colors (Linux Colors Konsole color scheme)
! Black + DarkGrey
*.color0:  #000000
*.color8:  #555753
! DarkRed + Red
*.color1:  #b21818
*.color9:  #ff5454
! DarkGreen + Green
*.color2:  #18b218
*.color10: #54ff54
! DarkYellow + Yellow
*.color3:  #b26818
*.color11: #ffff54
! DarkBlue + Blue
*.color4:  #1818b2
*.color12: #5454ff
! DarkMagenta + Magenta
*.color5:  #b218b2
*.color13: #ff54ff
! DarkCyan + Cyan
*.color6:  #18b2b2
*.color14: #54ffff
! LightGrey + White
*.color7:  #b2b2b2
*.color15: #ffffff

'' + (if urxvt == false then "" else ''
!!!
!!! URxvt
!!!

!! Colors
URxvt.foreground: #b2b2b2
URxvt.background: #000000

!! Scroll
URxvt.scrollWithBuffer: true
URxvt.scrollTtyKeypress: true
URxvt.saveLines: 10000

!! Appearance
URxvt.scrollBar: false
URxvt.borderLess: true
urxvt*depth: 32
urxvt*background: rgba:0000/0000/0000/af00

'')
