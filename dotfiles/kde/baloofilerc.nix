{ indexing ? true, hidden ? true, home ? "$HOME" }:
''
[Basic Settings]
Indexing-Enabled=${if indexing then "true" else "false"}

[General]
dbVersion=2
exclude filters=CMakeFiles,.obj,.npm,*.qrc,confstat,node_modules,.git,*.vm*,*.qmlc,.svn,*.omf,node_packages,*.part,CVS,*~,*.fasta,libtool,*.orig,*.faa,*.class,*.rej,CTestTestfile.cmake,*.pyc,nbproject,.moc,*.fna,.bzr,*.fastq,*.nvram,.xsession-errors*,*.pyo,*.ini,autom4te,CMakeTmp,*.a,*.loT,*.aux,.histfile.*,*.fq,qrc_*.cpp,Makefile.am,*.init,.pch,*.gmo,*.elc,*.so,litmain.sh,CMakeTmpQmake,*.o,.hg,_darcs,.uic,*.rcore,*.gb,conftest,po,*.m4,*.map,lzo,.yarn,*.swap,*.pc,.yarn-cache,confdefs.h,config.status,*.la,cmake_install.cmake,moc_*.cpp,*.po,*.gbff,ui_*.h,*.jsc,__pycache__,lost+found,CMakeCache.txt,*.lo,core-dumps,*.tmp,*.db,*.moc,*.csproj
exclude filters version=4
first run=false
exclude folders[$e]=${home}/tmp
index hidden folders=${if hidden then "true" else "false"}
''
