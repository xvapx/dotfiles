{ pkgs, api, dir }:
''
#!/usr/bin/env bash
  grive_sync(){
    ${pkgs.grive2}/bin/grive "$@" --id ${api.id} --secret ${api.secret}
    if [[ $? -ne 0 ]]; then
      grive_sync
    fi
  }
  pushd ${dir} && grive_sync && popd
''
