{ home ? "$HOME" }:
''
xml:readwrite:${home}/.gconf
xml:readonly:${home}/.nix-profile/etc/gconf/gconf.xml.defaults/
xml:readonly:/run/current-system/sw/etc/gconf/gconf.xml.defaults
''
