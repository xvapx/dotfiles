{ pkgs, home ? "$HOME" }:

let

  dirs = rec {
    nvim = "${home}/.config/nvim";
    undo = "${home}/.cache/vim/undo";
    autoload = "${nvim}/autoload";
    plugins = "${nvim}/plugged";
    vimwiki = "${home}/gdrive/IT/vimwiki";
  };

in ''
" Make sure the plugins directory exists
if !isdirectory("${dirs.plugins}")
  call mkdir("${dirs.plugins}", "p", 0700)
endif

" Check for vim-plug, copy it from the nix store if needed
if !filereadable('${dirs.autoload}/plug.vim')
  " Make sure the autoload directory exists
  if !isdirectory("${dirs.autoload}")
    call mkdir("${dirs.autoload}", "p", 0700)
  endif
  silent exec "!cp ${pkgs.vimPlugins.vim-plug}/plug.vim ${dirs.autoload}/"
  silent exec "!chmod 0700 ${dirs.autoload}/plug.vim"
endif

" Auto-install missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)')) | PlugInstall --sync | source $MYVIMRC | endif

" Load plugins with vim-plug
call plug#begin('${dirs.plugins}')
" Syntax checking
Plug 'dense-analysis/ale'
" Indent lines
Plug 'Yggdroot/indentLine'
" Status line
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Buffer explorer
Plug 'jlanzarotta/bufexplorer'
" Per-project editor configuration
Plug 'editorconfig/editorconfig-vim'
" Git integration
Plug 'tpope/vim-fugitive'
" Git diff
Plug 'mhinz/vim-signify'
Plug 'airblade/vim-gitgutter'
" Tree explorer
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
" Monokai color theme for Vim
Plug 'tomasr/molokai'
" Wimwiki
Plug 'vimwiki/vimwiki'
" fzf
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
" Language support
Plug 'LnL7/vim-nix'
Plug 'rust-lang/rust.vim'
Plug 'lervag/vimtex'
" Finish loading plugins
call plug#end()

" Plugin settings

" Ale error and warning signs.
let g:ale_sign_error = '⤫'
let g:ale_sign_warning = '⚠'
" Ale integration with airline.
let g:airline#extensions#ale#enabled = 1
" nixpkgs-fmt program for Ale
let g:ale_nix_nixpkgsfmt_executable = '${pkgs.nixpkgs-fmt}/bin/nixpkgs-fmt'
" airline plugin colors
let g:airline_theme = 'molokai'
" indent characters
let g:indentLine_char_list = ['|', '¦', '┆', '┊']
" do not hide urls
let g:indentLine_concealcursor=""
let g:indentLine_conceallevel=2
" gitgutter git location
let g:gitgutter_git_executable = '${pkgs.git}/bin/git'
" NerdTree autoclose
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" vimwiki
let g:vimwiki_list = [{'path': '${dirs.vimwiki}', 'syntax': 'markdown', 'ext': '.md'}]
let g:vimwiki_global_ext = 0
let g:vimwiki_url_maxsave = 0

" Basic settings
syntax on
filetype plugin indent on
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set colorcolumn=80
set smarttab
set autoindent
set smartindent
set smartcase
set ignorecase
set encoding=utf-8
set hidden
set history=1000
set number
set mouse=a
set clipboard=unnamedplus
set undodir=${dirs.undo}
set undofile 
set grepprg=${pkgs.ripgrep}/bin/rg\ --
set updatetime=100
" Autocomplete
set wildmode=list:longest,full
set wildmenu
" Markdown fenced code blocks
let g:markdown_fenced_languages = ['rust']

" Mappings
map <SPACE> <leader>

" Colors
set termguicolors
set background=dark
colorscheme molokai

" Make sure undo-dir exists
if !isdirectory("${dirs.undo}")
  call mkdir("${dirs.undo}", "p", 0700)
endif

" Enable the :T command to open a terminal on the bottom 12% of the window, map ESC to normal mode for terminal
command T below split | resize 12 | term
tnoremap <Esc> <C-\><C-n>

" fzf in vimwiki root directory
nnoremap <leader>fw :call fzf#run(
  \ {'options': '--reverse', 'dir': '~/gdrive/IT/vimwiki/', 'sink': 'tabedit' })<CR>
" rg in vimwiki root directory
command! -bang -nargs=* GrW call fzf#vim#grep(
  \ "rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 
  \ 1, {'options': '--delimiter : --nth 4.. --reverse', "dir": '~/gdrive/IT/vimwiki/'}, <bang>0)
nnoremap <leader>gw :GrW<CR>
" fzf in current file directory
nnoremap <leader>fh :call fzf#vim#files(
  \ expand('%:p:h'), {'options': '--reverse', 'sink': 'tabedit' })<CR>

" Automatically enter insert mode on entering any terminal window
autocmd TermOpen * startinsert

" Use ALT+{h,j,k,l} to navigate windows from any mode
tnoremap <A-h> <C-\><C-N><C-w>h
tnoremap <A-j> <C-\><C-N><C-w>j
tnoremap <A-k> <C-\><C-N><C-w>k
tnoremap <A-l> <C-\><C-N><C-w>l
inoremap <A-h> <C-\><C-N><C-w>h
inoremap <A-j> <C-\><C-N><C-w>j
inoremap <A-k> <C-\><C-N><C-w>k
inoremap <A-l> <C-\><C-N><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l
''
