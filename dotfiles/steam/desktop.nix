''
  [Desktop Entry]
  Name=Steam
  Comment=Application for managing and playing games
  Comment[ca]=Aplicació per gestionar Jocs
  Comment[es]=Aplicación para gestionar Juegos
  Exec=steam -silent %U
  Icon=steam
  Terminal=false
  Type=Application
  Categories=Network;FileTransfer;Game;
  MimeType=x-scheme-handler/steam;
  Actions=Store;Community;Library;Servers;Screenshots;News;Settings;BigPicture;Friends;

  [Desktop Action Store]
  Name=Store
  Name[ca]=Botiga
  Name[es]=Tienda
  Exec=steam steam://store

  [Desktop Action Community]
  Name=Community
  Name[ca]=Comunitat
  Name[es]=Comunidad
  Exec=steam steam://url/SteamIDControlPage

  [Desktop Action Library]
  Name=Library
  Name[ca]=Biblioteca
  Name[es]=Biblioteca
  Exec=steam steam://open/games

  [Desktop Action Servers]
  Name=Servers
  Name[ca]=Servidors
  Name[es]=Servidores
  Exec=steam steam://open/servers

  [Desktop Action Screenshots]
  Name=Screenshots
  Name[ca]=Captures
  Name[es]=Capturas
  Exec=steam steam://open/screenshots

  [Desktop Action News]
  Name=News
  Name[ca]=Notícies
  Name[es]=Noticias
  Exec=steam steam://open/news

  [Desktop Action Settings]
  Name=Settings
  Name[ca]=Preferències
  Name[es]=Preferencias
  Exec=steam steam://open/settings

  [Desktop Action BigPicture]
  Name=Big Picture
  Exec=steam steam://open/bigpicture

  [Desktop Action Friends]
  Name=Friends
  Name[ca]=Amics
  Name[es]=Amigos
  Exec=steam steam://open/friends
''
