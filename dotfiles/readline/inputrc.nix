''
  # Include any system-wide bindings and variable assignments from /etc/inputrc
  $include /etc/inputrc

  # Allow history searching matching already written text using up/down arrows.
  "\e[A": history-search-backward
  "\e[B": history-search-forward
''
