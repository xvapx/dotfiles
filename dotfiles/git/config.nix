{ email, name, sign ? false, sign-key ? "", editor ? "vim" }:

''
  [user]
    email = ${email}
    name = ${name}
    useconfigonly = true
    signingkey=${sign-key}

  [core]
      editor = "${editor}"
      whitespace = fix,-indent-with-non-tab,trailing-space

  [http]
      sslCAinfo = /etc/ssl/certs/ca-bundle.crt

  [gpg]
      program = gpg2

  [init]
      defaultBranch = main

  [commit]
      gpgSign = ${if sign then "true" else "false"}

  [branch]
      autosetupmerge = always
      autosetuprebase = always

  [pull]
    rebase = true

  [color]
      ui = auto
      branch = auto
      diff = auto
      grep = auto
      interactive = always
      pager = true
      showbranch = auto
      status = true

  [color "interactive"]
      error = red bold

  [color "branch"]
      current = yellow reverse
      local = yellow
      remote = green

  [color "diff"]
      meta = yellow
      frag = magenta
      old = red
      new = green
      whitespace = white reverse

  [color "status"]
      added = yellow
      changed = green
      untracked = cyan
      branch = magenta
''
