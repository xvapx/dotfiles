{ pkgs }:

{
  lib = import ./lib.nix { lib = pkgs.lib; };
  git.config = args@{ ... }: import ./dotfiles/git/config.nix ({} // args);
  neovim.init = args@{ ... }: import ./dotfiles/neovim/init.nix ({ inherit pkgs; } // args);
  tmux.conf = import ./dotfiles/tmux/conf.nix { inherit pkgs; };
  readline = {
    inputrc = import ./dotfiles/readline/inputrc.nix;
  };
  bash = {
    profile = import ./dotfiles/bash/profile.nix;
    rc = args@{ ... }: import ./dotfiles/bash/rc.nix ({ inherit pkgs; } // args);
    aliases = args@{ ... }: import ./dotfiles/bash/aliases.nix ({ inherit pkgs; } // args);
    locale = import ./dotfiles/locale.nix;
  };
  bin = {
    gdrive = args@{ ... }: import ./dotfiles/bin/gdrive.nix ({ inherit pkgs; } // args);
  };
  xorg = {
    xdefaults = args@{ ... }: import ./dotfiles/xorg/xdefaults.nix ({ } // args);
    xresources = args@{ ... }: import ./dotfiles/xorg/xdefaults.nix ({ } // args);
  };
  kde = {
    baloofilerc = args@{ ... }: import ./dotfiles/kde/baloofilerc.nix ({ } // args);
  };
  steam = {
    desktop = import ./dotfiles/steam/desktop.nix;
  };
}
