{ config, lib, options, pkgs, ... }:

with lib;

let

  dotfiles = import ./dotfiles.nix {
    inherit pkgs;
  };

  modules = import ./modules.nix {
    inherit lib pkgs dotfiles;
    cfg = config.marti.dotfiles;
    hostname = config.networking.hostName;
    secrets = config.age.secrets or {};
  };

in

{

  imports = [
    modules
  ];

}
